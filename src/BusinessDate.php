<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Class BusinessDate.
 *
 * @package Drupal\d01_drupal_business_hours
 */
class BusinessDate implements BusinessDateInterface {

  use BusinessHoursTrait;

  /**
   * The date.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime
   */
  protected $date = NULL;

  /**
   * List of Business Hour objects.
   *
   * @var \Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface[]
   */
  protected $periods = [];

  /**
   * List bundled comments.
   *
   * @var \Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface[]
   */
  protected $bundledComments = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(DrupalDateTime $date, array $periods = []) {
    $this->date = $date;
    $this->periods = $this->preparePeriods($periods);
    $this->bundledComments = $this->bundleComments($periods);
  }

  /**
   * {@inheritdoc}
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * {@inheritdoc}
   */
  public function getPeriods() {
    return $this->periods;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundledComments() {
    return $this->bundledComments;
  }

  /**
   * {@inheritdoc}
   */
  public function isOpenNow() {
    // When day is closed we can just return false.
    if ($this->isClosedToday()) {
      return FALSE;
    }

    // Loop periods.
    foreach ($this->getPeriods() as $period) {
      $from = $period->getFromTime()->getTimestamp();
      $until = $period->getUntilTime()->getTimestamp();

      if (($from <= $this->getNow()->getTimestamp()) && !($until < $this->getNow()->getTimestamp())) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosedNow() {
    return !$this->isOpenNow();
  }

  /**
   * {@inheritdoc}
   */
  public function isOpenToday() {
    return !$this->isClosedToday();
  }

  /**
   * {@inheritdoc}
   */
  public function isClosedToday() {
    return empty($this->getPeriods()) ? TRUE : FALSE;
  }

  /**
   * Bundle the comments.
   *
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface[] $periods
   *   List of business date periods.
   *
   * @return \Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface[]
   *   Ordered list of business date periods by day.
   */
  private function bundleComments(array $periods) {
    $comments = [];

    // We make sure we always create the periods in the
    // correct chronological order.
    if (!empty($periods)) {
      foreach ($periods as $period) {

        // Make sure the period passed matches the date.
        if (!$this->isSameDate($this->getDate(), $period->getDate())) {
          continue;
        }

        $comments[] = $period->getComment();
      }
    }

    return $comments;
  }

  /**
   * Prepare the business periods.
   *
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface[] $periods
   *   List of business date periods.
   *
   * @return \Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface[]
   *   Ordered list of business date periods by day.
   */
  private function preparePeriods(array $periods) {
    $ordered = [];

    // We make sure we always create the periods in the
    // correct chronological order.
    if (!empty($periods)) {
      foreach ($periods as $period) {

        // When a closing period is passed, all other will be ignore
        // We will mark the date as closed.
        if (!$period->getFromTime()) {
          $ordered = [];
          break;
        }

        // Make sure the period passed matches the date.
        if (!$this->isSameDate($this->getDate(), $period->getDate())) {
          continue;
        }

        // Set timestamp as key for easy sorting.
        $timestamp = $period->getFromTime()->getTimestamp();
        $ordered[$timestamp] = $period;
      }

      // Sort by timestamp.
      ksort($ordered);

      // Clean up the keys.
      $periods = [];
      $count = 0;
      foreach ($ordered as $key => $order) {
        $periods[$count] = $order;
        $count++;
      }

      $ordered = $periods;
    }

    return $ordered;
  }

}
