<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\d01_drupal_time\D01DrupalTime;

/**
 * Class BusinessPeriod.
 *
 * @package Drupal\d01_drupal_business_hours
 */
class BusinessPeriod implements BusinessPeriodInterface {

  /**
   * D01 Drupal Time object.
   *
   * @var \Drupal\d01_drupal_time\D01DrupalTime\null
   */
  protected $from = NULL;

  /**
   * D01 Drupal Time object.
   *
   * @var \Drupal\d01_drupal_time\D01DrupalTime\null
   */
  protected $until = NULL;

  /**
   * Comment.
   *
   * @var string
   */
  protected $comment = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(D01DrupalTime $from = NULL, D01DrupalTime $until = NULL, $comment = NULL) {
    $this->from = $from;
    $this->until = $until;
    $this->comment = $comment;
  }

  /**
   * {@inheritdoc}
   */
  public function getFromTime() {
    return $this->from;
  }

  /**
   * {@inheritdoc}
   */
  public function getUntilTime() {
    return $this->until;
  }

  /**
   * {@inheritdoc}
   */
  public function getComment() {
    return $this->comment;
  }

}
