<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Interface BusinessWeekInterface.
 *
 * @package Drupal\d01_drupal_business_hours
 */
interface BusinessWeekInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDayInterface[] $business_hours
   *   An array of items implementing the business hour interface.
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface[] $exceptions
   *   An array of items implementing the business exception interface.
   */
  public function __construct(array $business_hours, array $exceptions);

  /**
   * Set the first day of the week.
   *
   * Starting from "0" (Sunday) until
   * "6" (Saturday). See PHP Date.
   *
   * @param int $first_day
   *   A numeric representation of day.
   */
  public function setFirstDay($first_day);

  /**
   * Get only the open business days.
   *
   * @return array
   *   A array of Business Day objects.
   */
  public function getOpenBusinessDays();

  /**
   * Get only the closed business days.
   *
   * @return array
   *   A array of Business Day objects.
   */
  public function getClosedBusinessDays();

  /**
   * Get all business days.
   *
   * @return array
   *   A array of Business Day objects.
   */
  public function getBusinessDays();

  /**
   * Get all business days exceptions.
   *
   * @return array
   *   A array of Business Date objects.
   */
  public function getBusinessExceptions();

  /**
   * Check if open now on this instance.
   *
   * @return bool
   *   A boolean indication if open.
   */
  public function isOpenNow();

  /**
   * Check if closed now on this instance.
   *
   * @return bool
   *   A boolean indication if closed.
   */
  public function isClosedNow();

  /**
   * Check if open somewhere this day.
   *
   * @return bool
   *   A boolean indication if open.
   */
  public function isOpenToday();

  /**
   * Check if closed this day (whole day).
   *
   * @return bool
   *   A boolean indication if closed.
   */
  public function isClosedToday();

  /**
   * Check if open somewhere this day.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $date
   *   The date to check.
   *
   * @return bool
   *   A boolean indication if open.
   */
  public function isOpenOnDate(DrupalDateTime $date = NULL);

  /**
   * Check if closed this day (whole day).
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $date
   *   The date to check.
   *
   * @return bool
   *   A boolean indication if closed.
   */
  public function isClosedOnDate(DrupalDateTime $date = NULL);

}
