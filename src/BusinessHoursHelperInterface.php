<?php

namespace Drupal\d01_drupal_business_hours;

/**
 * Interface BusinessHoursHelperInterface.
 *
 * @package Drupal\d01_drupal_business_hours
 */
interface BusinessHoursHelperInterface {

  /**
   * Get the business week.
   *
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDayInterface[] $business_periods
   *   An array of business periods.
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface[] $business_exception_periods
   *   An array of business periods exceptions.
   * @param bool $exclude_past_dates
   *   Whether or not dates in the past should be excluded.
   *
   * @return \Drupal\d01_drupal_business_hours\BusinessWeek
   *   A business week object this object will handle all date calculations.
   */
  public function getBusinessWeek(array $business_periods = [], array $business_exception_periods = [], $exclude_past_dates = TRUE);

}
