<?php

namespace Drupal\d01_drupal_business_hours\Plugin\Field\FieldFormatter;

use Drupal\d01_drupal_business_hours\BusinessWeek;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Formatter for "Period (Day)" field.
 *
 * @FieldFormatter(
 *   id = "d01_drupal_business_hours_period_day_formatter",
 *   label = @Translation("Period (Day) Formatter"),
 *   field_types = {
 *     "d01_drupal_business_hours_period_day"
 *   }
 * )
 */
class BusinessPeriodDayFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $config = \Drupal::config('system.date');

    $business_hours = [];
    foreach ($items as $item) {
      $business_hours[] = $item->value;
    }

    $business_week = new BusinessWeek($business_hours);
    $business_days = $business_week->getBusinessDays();

    foreach ($business_days as $delta => $business_day) {

      $times = [];
      if (!$business_day->isClosedToday()) {
        $periods = $business_day->getPeriods();
        foreach ($periods as $period) {
          $comment = $period->getComment();

          $times[] = [
            '#theme' => 'd01_drupal_business_hours_period',
            '#from' => $period->getFromTime()
              ->format('H:i'),
            '#until' => $period->getUntilTime()
              ->format('H:i'),
            '#comment' => $comment,
          ];
        }
      }

      $element[$delta] = [
        '#theme' => 'd01_drupal_business_hours_day',
        '#day' => $business_day->label(),
        '#is_closed' => $business_day->isClosedToday(),
        '#periods' => $times,
      ];
    }

    return $element;
  }

}
