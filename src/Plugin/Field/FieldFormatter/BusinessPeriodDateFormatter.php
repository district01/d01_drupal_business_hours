<?php

namespace Drupal\d01_drupal_business_hours\Plugin\Field\FieldFormatter;

use Drupal\d01_drupal_business_hours\BusinessWeek;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Formatter for "Period (Date)" field.
 *
 * @FieldFormatter(
 *   id = "d01_drupal_business_hours_period_date_formatter",
 *   label = @Translation("Period (Date) Formatter"),
 *   field_types = {
 *     "d01_drupal_business_hours_period_date"
 *   }
 * )
 */
class BusinessPeriodDateFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $config = \Drupal::config('system.date');
    $timezone = new \DateTimeZone($config->get('timezone.default'));

    $dates = [];
    foreach ($items as $item) {
      $dates[] = $item->value;
    }

    $business_week = new BusinessWeek([], $dates);
    $exception_days = $business_week->getOpenBusinessExceptions();

    foreach ($exception_days as $delta => $exception_day) {
      $times = [];

      $periods = $exception_day->getPeriods();
      foreach ($periods as $period) {

        $comment = $period->getComment();

        $times[] = [
          '#theme' => 'd01_drupal_business_hours_period',
          '#from' => $period->getFromTime()
            ->format('H:i'),
          '#until' => $period->getUntilTime()
            ->format('H:i'),
          '#comment' => $comment,
        ];
      }

      $element[$delta] = [
        '#theme' => 'd01_drupal_business_hours_date',
        '#date' => $exception_day->getDate()->setTimeZone($timezone)->format('d-m-Y'),
        '#is_closed' => $exception_day->isClosedToday(),
        '#periods' => $times,
      ];
    }

    return $element;
  }

}
