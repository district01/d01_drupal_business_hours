<?php

namespace Drupal\d01_drupal_business_hours\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\d01_drupal_business_hours\BusinessWeek;

/**
 * Formatter for "Closing Date" field.
 *
 * @FieldFormatter(
 *   id = "d01_drupal_business_hours_closing_date_formatter",
 *   label = @Translation("Closing Date Formatter"),
 *   field_types = {
 *     "d01_drupal_business_hours_closing_date"
 *   }
 * )
 */
class BusinessClosingDateFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $config = \Drupal::config('system.date');
    $timezone = new \DateTimeZone($config->get('timezone.default'));

    $dates = [];
    foreach ($items as $item) {
      $dates[] = $item->value;
    }

    $business_week = new BusinessWeek([], $dates);
    $exception_days = $business_week->getClosedBusinessExceptions();

    foreach ($exception_days as $delta => $exception_day) {
      $comments = $exception_day->getBundledComments();

      $text = '';
      foreach ($comments as $comment) {
        $text .= $comment . ' ';
      }

      // Render each element as markup.
      $element[$delta] = [
        '#theme' => 'd01_drupal_business_hours_date',
        '#date' => $exception_day->getDate()->setTimeZone($timezone)->format('d-m-Y'),
        '#is_closed' => $exception_day->isClosedToday(),
        '#periods' => [],
        '#comments' => $text,
      ];
    }

    return $element;
  }

}
