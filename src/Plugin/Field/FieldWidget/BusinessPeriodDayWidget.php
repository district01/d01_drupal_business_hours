<?php

namespace Drupal\d01_drupal_business_hours\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DateHelper;
use Drupal\d01_drupal_business_hours\Plugin\Field\FieldType\BusinessPeriodDayItem;
use Drupal\d01_drupal_time\D01DrupalTime;

/**
 * Widget for "Period (Day)" field.
 *
 * @FieldWidget(
 *   id = "d01_drupal_business_hours_period_day_widget",
 *   label = @Translation("Period (Day) Widget"),
 *   field_types = {
 *     "d01_drupal_business_hours_period_day"
 *   }
 * )
 */
class BusinessPeriodDayWidget extends WidgetBase implements WidgetInterface {

  const DEFAULT_INTERVAL = 30;
  const DEFAULT_TIME_FORMAT = 'HH:mm';
  const ALLOW_COMMENTS = TRUE;
  const TIME_FORMAT_OPTIONS = [
    'h:m' => 'h:m',
    'h:mm' => 'h:mm',
    'hh:mm' => 'hh:mm',
    'H:m' => 'H:m',
    'H:mm' => 'H:mm',
    'HH:mm' => 'HH:mm',
    'h:m p' => 'h:m p',
    'h:mm p' => 'h:mm p',
    'hh:mm p' => 'hh:mm p',
    'H:m p' => 'H:m p',
    'H:mm p' => 'H:mm p',
    'HH:mm p' => 'HH:mm p',
  ];

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'interval' => BusinessPeriodDayWidget::DEFAULT_INTERVAL,
      'time_format' => BusinessPeriodDayWidget::DEFAULT_TIME_FORMAT,
      'enable_comments' => BusinessPeriodDayWidget::ALLOW_COMMENTS,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Interval: @interval, Time format: @time_format, Allow comments: @enable_comments', [
      '@interval' => $this->getSetting('interval'),
      '@time_format' => $this->getSetting('time_format'),
      '@enable_comments' => $this->getSetting('enable_comments') ? t('Yes') : t('No'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['interval'] = [
      '#type' => 'number',
      '#title' => t('The interval in minutes'),
      '#default_value' => $this->getSetting('interval'),
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 60,
    ];

    $element['time_format'] = [
      '#type' => 'select',
      '#options' => BusinessPeriodDayWidget::TIME_FORMAT_OPTIONS,
      '#title' => t('The javascript time format'),
      '#default_value' => $this->getSetting('time_format'),
      '#required' => TRUE,
    ];

    $element['enable_comments'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow the user to provide comments'),
      '#default_value' => $this->getSetting('enable_comments'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [
      '#type' => 'fieldset',
    ];

    $default_day = isset($items[$delta]->day) ? $items[$delta]->day : NULL;

    $element['day'] = [
      '#title' => t('Day'),
      '#type' => 'select',
      '#options' => DateHelper::weekDaysOrdered(DateHelper::weekDays(TRUE)),
      '#default_value' => $default_day,
    ];

    $default_from = isset($items[$delta]) && ($items[$delta] instanceof BusinessPeriodDayItem) ? $items[$delta]->from : NULL;
    $default_until = isset($items[$delta]) && ($items[$delta] instanceof BusinessPeriodDayItem) ? $items[$delta]->until : NULL;

    $config = \Drupal::config('system.date');
    $element['time_range'] = [
      '#type' => 'd01_drupal_time_range_picker',
      '#js_settings' => [
        'timeFormat' => $this->getSetting('time_format') ? $this->getSetting('time_format') : BusinessPeriodDayWidget::DEFAULT_TIME_FORMAT,
        'interval' => $this->getSetting('interval') ? $this->getSetting('interval') : BusinessPeriodDayWidget::DEFAULT_INTERVAL,
        'scrollbar' => TRUE,
      ],
      '#default_value' => [
        'from' => $default_from,
        'until' => $default_until,
      ],
      '#timezone_display' => $config->get('timezone.default'),
    ];

    if ($this->getSetting('enable_comments')) {
      $comment = isset($items[$delta]->comment) ? $items[$delta]->comment : NULL;

      $element['comment'] = [
        '#title' => t('Comment'),
        '#type' => 'textfield',
        '#default_value' => $comment,
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $new_values = [];
    foreach ($values as $delta => $value) {

      // Set the day.
      $new_values[$delta]['day'] = $value['day'];

      // Set the comment when we support comments.
      if ($this->getSetting('enable_comments')) {
        $new_values[$delta]['comment'] = $value['comment'];
      }
      else {
        // Make sure we reset comment when it's not allowed.
        $new_values[$delta]['comment'] = FALSE;
      }

      // The d01_drupal_time_range_picker form element type has transformed
      // the value to a DrupalDateTime object at this point. We need to
      // convert it back to the storage timezone and format.
      if (!empty($value['time_range']['from']) && $value['time_range']['from'] instanceof D01DrupalTime) {
        $from = $value['time_range']['from'];

        // Adjust the date for storage in storage format.
        $new_values[$delta]['from_value'] = $from->format(BusinessPeriodDayItem::DEFAULT_STORAGE_FORMAT);
      }
      else {
        $new_values[$delta]['from_value'] = FALSE;
      }

      // The d01_drupal_time_range_picker form element type has transformed
      // the value to a DrupalDateTime object at this point. We need to
      // convert it back to the storage timezone and format.
      if (!empty($value['time_range']['until']) && $value['time_range']['until'] instanceof D01DrupalTime) {
        $until = $value['time_range']['until'];

        // Adjust the date for storage in storage format.
        $new_values[$delta]['until_value'] = $until->format(BusinessPeriodDayItem::DEFAULT_STORAGE_FORMAT);
      }
      else {
        $new_values[$delta]['until_value'] = FALSE;
      }
    }

    return $new_values;
  }

}
