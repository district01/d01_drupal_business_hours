<?php

namespace Drupal\d01_drupal_business_hours\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\d01_drupal_business_hours\Plugin\Field\FieldType\BusinessClosingDateItem;

/**
 * Widget for "Closing Date" field.
 *
 * @FieldWidget(
 *   id = "d01_drupal_business_hours_closing_date_widget",
 *   label = @Translation("Closing Date Widget"),
 *   field_types = {
 *     "d01_drupal_business_hours_closing_date"
 *   }
 * )
 */
class BusinessClosingDateWidget extends WidgetBase implements WidgetInterface {

  const ALLOW_COMMENTS = TRUE;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'enable_comments' => BusinessClosingDateWidget::ALLOW_COMMENTS,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Allow comments: @enable_comments', [
      '@enable_comments' => $this->getSetting('enable_comments') ? t('Yes') : t('No'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['enable_comments'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow the user to provide comments'),
      '#default_value' => $this->getSetting('enable_comments'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [
      '#type' => 'fieldset',
    ];

    $default_date = isset($items[$delta]->date) ? $items[$delta]->date : NULL;
    $element['date'] = [
      '#title' => t('Date'),
      '#type' => 'datetime',
      '#date_time_element' => 'none',
      '#default_value' => $default_date,
      '#date_increment' => 1,
    ];

    if ($this->getSetting('enable_comments')) {
      $comment = isset($items[$delta]->comment) ? $items[$delta]->comment : NULL;

      $element['comment'] = [
        '#title' => t('Comment'),
        '#type' => 'textfield',
        '#default_value' => $comment,
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $new_values = [];

    foreach ($values as $delta => $value) {

      // The datetime form element type has transformed
      // the value to a DrupalDateTime object at this point. We need to
      // convert it back to the storage timezone and format.
      if (!empty($value['date']) && $value['date'] instanceof DrupalDateTime) {
        $date = $value['date'];

        // Adjust the date for storage in storage format.
        $date->setTimezone(new \DateTimezone(BusinessClosingDateItem::DEFAULT_STORAGE_TIMEZONE));
        $new_values[$delta]['date_value'] = $date->format(BusinessClosingDateItem::DEFAULT_STORAGE_FORMAT);
      }

      // Set the comment when we support comments.
      if ($this->getSetting('enable_comments')) {
        $new_values[$delta]['comment'] = $value['comment'];
      }
      else {
        // Make sure we reset comment when it's not allowed.
        $new_values[$delta]['comment'] = FALSE;
      }
    }

    return $new_values;
  }

}
