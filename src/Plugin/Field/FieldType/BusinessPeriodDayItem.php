<?php

namespace Drupal\d01_drupal_business_hours\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Datetime\DateHelper;
use Drupal\d01_drupal_business_hours\BusinessPeriodInterface;
use Drupal\d01_drupal_business_hours\BusinessPeriodDayInterface;

/**
 * Provides a field type for saving day based periods.
 *
 * @FieldType(
 *   id = "d01_drupal_business_hours_period_day",
 *   label = @Translation("Business Hours"),
 *   category = @Translation("Business hours"),
 *   default_formatter = "d01_drupal_business_hours_period_day_formatter",
 *   default_widget = "d01_drupal_business_hours_period_day_widget",
 * )
 */
class BusinessPeriodDayItem extends FieldItemBase implements FieldItemInterface {

  const DEFAULT_STORAGE_TIMEZONE = 'UTC';
  const DEFAULT_STORAGE_FORMAT = 'Y-m-d\TH:i:s';

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'day' => [
          'type' => 'int',
          'not null' => FALSE,
        ],
        'from_value' => [
          'description' => 'The from value.',
          'type' => 'varchar',
          'length' => 20,
        ],
        'until_value' => [
          'description' => 'The until value.',
          'type' => 'varchar',
          'length' => 20,
        ],
        'comment' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['value'] = DataDefinition::create('any')
      ->setLabel(t('Computed value'))
      ->setDescription(t('The computed BusinessPeriodDay object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\d01_drupal_business_hours\BusinessPeriodDayComputed')
      ->setSetting('day', 'day')
      ->setSetting('from', 'from')
      ->setSetting('until', 'until')
      ->setSetting('comment', 'comment');

    $properties['day'] = DataDefinition::create('integer')
      ->setLabel(t('Day'))
      ->setDescription('Stores the numeric representation of a day (0-6)');

    $properties['from_value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('From value'))
      ->setRequired(TRUE);

    $properties['from'] = DataDefinition::create('any')
      ->setLabel(t('Computed from'))
      ->setDescription(t('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\d01_drupal_business_hours\BusinessD01DrupalTimeComputed')
      ->setSetting('date source', 'from_value');

    $properties['until_value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Until'))
      ->setRequired(TRUE);

    $properties['until'] = DataDefinition::create('any')
      ->setLabel(t('Computed until'))
      ->setDescription(t('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\d01_drupal_business_hours\BusinessD01DrupalTimeComputed')
      ->setSetting('date source', 'until_value');

    $properties['comment'] = DataDefinition::create('string')
      ->setLabel(t('Comment'))
      ->addConstraint('Length', ['max' => 255]);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {

    // Since "0" as string is a valid value ("Sunday")
    // We need to check for value this way.
    if (!$this->get('day')->getValue() && $this->get('day')->getValue() !== '0') {
      return TRUE;
    }

    // When no valid day is set we consider the field to be empty.
    if (!in_array(intval($this->get('day')->getValue()), array_keys(DateHelper::weekDays(TRUE)))) {
      return TRUE;
    }

    // When one of both range values isn't set we consider
    // the field to be empty.
    if (empty($this->get('from_value')->getValue()) || empty($this->get('until_value')->getValue())) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {

    // Enforce that the computed date is recalculated.
    if ($property_name == 'from_value') {
      $this->from = NULL;
      $this->value = NULL;
    }

    // Enforce that the computed date is recalculated.
    if ($property_name == 'until_value') {
      $this->until = NULL;
      $this->value = NULL;
    }

    // Enforce that the computed date is recalculated.
    if ($property_name == 'day') {
      $this->value = NULL;
    }

    // Enforce that the computed date is recalculated.
    if ($property_name == 'comment') {
      $this->value = NULL;
    }

    parent::onChange($property_name, $notify);
  }

}
