<?php

namespace Drupal\d01_drupal_business_hours\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\d01_drupal_business_hours\BusinessPeriodInterface;
use Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface;

/**
 * Provides a field type for saving date based closings.
 *
 * @FieldType(
 *   id = "d01_drupal_business_hours_closing_date",
 *   label = @Translation("Closing Dates"),
 *   category = @Translation("Business hours"),
 *   default_formatter = "d01_drupal_business_hours_closing_date_formatter",
 *   default_widget = "d01_drupal_business_hours_closing_date_widget",
 * )
 */
class BusinessClosingDateItem extends FieldItemBase implements FieldItemInterface {

  const DEFAULT_STORAGE_TIMEZONE = 'UTC';
  const DEFAULT_STORAGE_FORMAT = 'Y-m-d\TH:i:s';

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'date_value' => [
          'description' => 'The date value.',
          'type' => 'varchar',
          'length' => 20,
        ],
        'comment' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['value'] = DataDefinition::create('any')
      ->setLabel(t('Computed value'))
      ->setDescription(t('The computed BusinessPeriodDate object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\d01_drupal_business_hours\BusinessPeriodDateComputed')
      ->setSetting('date', 'date')
      ->setSetting('comment', 'comment');

    $properties['date_value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Date value'))
      ->setRequired(TRUE);

    $properties['date'] = DataDefinition::create('any')
      ->setLabel(t('Computed date'))
      ->setDescription(t('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\datetime\DateTimeComputed')
      ->setSetting('date source', 'date_value');

    $properties['comment'] = DataDefinition::create('string')
      ->setLabel(t('Comment'))
      ->addConstraint('Length', ['max' => 255]);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if (empty($this->get('date_value')->getValue())) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    // Enforce that the computed date is recalculated.
    if ($property_name == 'date_value') {
      $this->date = NULL;
      $this->value = NULL;
    }

    // Enforce that the computed date is recalculated.
    if ($property_name == 'comment') {
      $this->value = NULL;
    }

    parent::onChange($property_name, $notify);
  }

}
