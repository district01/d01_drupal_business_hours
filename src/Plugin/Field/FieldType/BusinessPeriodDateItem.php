<?php

namespace Drupal\d01_drupal_business_hours\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\d01_drupal_business_hours\BusinessPeriodInterface;
use Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface;

/**
 * Provides a field type for saving date based periods.
 *
 * @FieldType(
 *   id = "d01_drupal_business_hours_period_date",
 *   label = @Translation("Exceptional Business Hours"),
 *   category = @Translation("Business hours"),
 *   default_formatter = "d01_drupal_business_hours_period_date_formatter",
 *   default_widget = "d01_drupal_business_hours_period_date_widget",
 * )
 */
class BusinessPeriodDateItem extends FieldItemBase implements FieldItemInterface {

  const DEFAULT_STORAGE_TIMEZONE = 'UTC';
  const DEFAULT_STORAGE_FORMAT = 'Y-m-d\TH:i:s';

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'date_value' => [
          'description' => 'The date value.',
          'type' => 'varchar',
          'length' => 20,
        ],
        'from_value' => [
          'description' => 'The from value.',
          'type' => 'varchar',
          'length' => 20,
        ],
        'until_value' => [
          'description' => 'The until value.',
          'type' => 'varchar',
          'length' => 20,
        ],
        'comment' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['value'] = DataDefinition::create('any')
      ->setLabel(t('Computed value'))
      ->setDescription(t('The computed BusinessPeriodDate object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\d01_drupal_business_hours\BusinessPeriodDateComputed')
      ->setSetting('date', 'date')
      ->setSetting('from', 'from')
      ->setSetting('until', 'until')
      ->setSetting('comment', 'comment');

    $properties['date_value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Date value'))
      ->setRequired(TRUE);

    $properties['date'] = DataDefinition::create('any')
      ->setLabel(t('Computed date'))
      ->setDescription(t('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\datetime\DateTimeComputed')
      ->setSetting('date source', 'date_value');

    $properties['from_value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('From value'))
      ->setRequired(TRUE);

    $properties['from'] = DataDefinition::create('any')
      ->setLabel(t('Computed from'))
      ->setDescription(t('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\d01_drupal_business_hours\BusinessD01DrupalTimeComputed')
      ->setSetting('date source', 'from_value');

    $properties['until_value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Until'))
      ->setRequired(TRUE);

    $properties['until'] = DataDefinition::create('any')
      ->setLabel(t('Computed until'))
      ->setDescription(t('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\d01_drupal_business_hours\BusinessD01DrupalTimeComputed')
      ->setSetting('date source', 'until_value');

    $properties['comment'] = DataDefinition::create('string')
      ->setLabel(t('Comment'))
      ->addConstraint('Length', ['max' => 255]);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if (empty($this->get('date_value')->getValue()) || empty($this->get('from_value')->getValue()) || empty($this->get('until_value')->getValue())) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    // Enforce that the computed date is recalculated.
    if ($property_name == 'date_value') {
      $this->date = NULL;
      $this->value = NULL;
    }

    // Enforce that the computed date is recalculated.
    if ($property_name == 'from_value') {
      $this->from = NULL;
      $this->value = NULL;
    }

    // Enforce that the computed date is recalculated.
    if ($property_name == 'until_value') {
      $this->until = NULL;
      $this->value = NULL;
    }

    // Enforce that the computed date is recalculated.
    if ($property_name == 'comment') {
      $this->value = NULL;
    }

    parent::onChange($property_name, $notify);
  }

}
