<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Interface BusinessDayExceptionInterface.
 *
 * @package Drupal\d01_drupal_business_hours
 */
interface BusinessDateInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A date time object.
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface[] $periods
   *   An array of periods of type 'date'.
   */
  public function __construct(DrupalDateTime $date, array $periods = []);

  /**
   * Get the Date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   a drupal date time object.
   */
  public function getDate();

  /**
   * Get the periods of the Day.
   *
   * @return \Drupal\d01_drupal_business_hours\BusinessPeriodInterface[]
   *   An array of Business hours.
   */
  public function getPeriods();

  /**
   * Get the comments of the Day.
   *
   * @return array
   *   An array of Text strings.
   */
  public function getBundledComments();

  /**
   * Check if the date is closed today (whole day).
   *
   * This function will only return TRUE, when there are
   * no periods for this date. It will also take
   * the passed exceptions into account. This function will
   * not check in real-time, use isOpenNow()
   * for that.
   *
   * @return bool
   *   Boolean indicating if this date is closed.
   */
  public function isClosedToday();

  /**
   * Check if the date is open today (whole day).
   *
   * This will only return TRUE, when there are
   * periods for this date. This function will
   * not check in real-time, use isOpenNow()
   * for that.
   *
   * @return bool
   *   Boolean indicating if this date is open today.
   */
  public function isOpenToday();

  /**
   * Check if the date is closed on this instance.
   *
   * This will check in real-time, to see if date is closed.
   *
   * @return bool
   *   Boolean indicating if this date is closed today.
   */
  public function isClosedNow();

  /**
   * Check if the date is open on this instance.
   *
   * This will check in real-time, to see if date is open.
   *
   * @return bool
   *   Boolean indicating if this date is open today.
   */
  public function isOpenNow();

}
