<?php

namespace Drupal\d01_drupal_business_hours;

/**
 * Class BusinessHoursHelper.
 *
 * @package Drupal\d01_drupal_business_hours
 */
class BusinessHoursHelper implements BusinessHoursHelperInterface {

  /**
   * {@inheritdoc}
   */
  public function getBusinessWeek(array $business_periods = [], array $business_exception_periods = [], $exclude_past_dates = TRUE) {
    return new BusinessWeek($business_periods, $business_exception_periods, $exclude_past_dates);
  }

}
