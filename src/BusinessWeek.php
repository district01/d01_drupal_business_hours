<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\DateHelper;

/**
 * Class BusinessWeek.
 *
 * @package Drupal\d01_drupal_business_hours
 */
class BusinessWeek implements BusinessWeekInterface {

  use BusinessHoursTrait;

  /**
   * Numeric representation of day.
   *
   * @var int
   */
  protected $firstDay = NULL;

  /**
   * List of Business Day objects.
   *
   * @var \Drupal\d01_drupal_business_hours\BusinessDayInterface[]
   */
  protected $businessDays = [];

  /**
   * List of Business Date objects.
   *
   * @var \Drupal\d01_drupal_business_hours\BusinessDateInterface[]
   */
  protected $businessExceptions = [];

  /**
   * Boolean indicating if passed dates should be excluded.
   *
   * @var bool
   */
  protected $excludePastDates = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $business_periods = [], array $business_exception_periods = [], $exclude_past_dates = TRUE) {
    $this->firstDay = $this->getFirstDayOfWeek();
    $this->businessDays = $this->prepareBusinessDays($business_periods);
    $this->businessExceptions = $this->prepareBusinessExceptions($business_exception_periods);
    $this->excludePastDates = $exclude_past_dates;
  }

  /**
   * {@inheritdoc}
   */
  public function setFirstDay($first_day) {
    if (in_array($first_day, range(0, 6))) {
      $this->firstDay = $first_day;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBusinessDays() {
    $business_days = $this->businessDays;

    $ordered = [];
    foreach ($business_days as $business_day) {
      // Build key based on day so we can group items by day.
      // By using this format we can also ksort the value by day.
      $day = $business_day->getDay();
      $key = $day;

      $ordered[$key] = $business_day;
    }

    // Sort from 0 to 6.
    ksort($ordered);

    // Make sure we start from the set first day of the week.
    $key_sorted = $ordered;
    $ordered = [];
    foreach ($this->getOrderedWeekDays() as $key => $weekday) {
      if (!$key_sorted[$key]) {
        continue;
      }
      $ordered[$key] = $key_sorted[$key];
    };

    // Set the business days.
    $this->businessDays = $ordered;
    return $this->businessDays;
  }

  /**
   * {@inheritdoc}
   */
  public function getOpenBusinessDays() {
    $business_days = [];
    foreach ($this->getBusinessDays() as $business_day) {
      if (!$business_day->isClosed()) {
        $business_days[] = $business_day;
      }
    }
    return $business_days;
  }

  /**
   * {@inheritdoc}
   */
  public function getClosedBusinessDays() {
    $business_days = [];
    foreach ($this->getBusinessDays() as $business_day) {
      if ($business_day->isClosed()) {
        $business_days[] = $business_day;
      }
    }
    return $business_days;
  }

  /**
   * {@inheritdoc}
   */
  public function getBusinessExceptions($include_past = FALSE) {
    return $this->businessExceptions;
  }

  /**
   * {@inheritdoc}
   */
  public function getOpenBusinessExceptions($include_past = FALSE) {
    $exceptions = [];
    foreach ($this->getBusinessExceptions($include_past) as $exception) {
      if (!$exception->isClosedToday()) {
        $exceptions[] = $exception;
      }
    }
    return $exceptions;
  }

  /**
   * {@inheritdoc}
   */
  public function getClosedBusinessExceptions($include_past = FALSE) {
    $exceptions = [];
    foreach ($this->getBusinessExceptions($include_past) as $exception) {
      if ($exception->isClosedToday()) {
        $exceptions[] = $exception;
      }
    }
    return $exceptions;
  }

  /**
   * {@inheritdoc}
   */
  public function isOpenNow() {
    // First check if we have exceptions overriding the normal
    // opening days.
    foreach ($this->getBusinessExceptions() as $exception) {
      if (!$this->isSameDate($exception->getDate(), $this->getToday())) {
        continue;
      }

      return $exception->isOpenNow();
    }

    // Then check the current day for hours.
    // opening days.
    foreach ($this->getBusinessDays() as $business_day) {
      if (intval($this->getToday()->format('w')) !== $business_day->getDay()) {
        continue;
      }

      return $business_day->isOpenNow();
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosedNow() {
    return !$this->isOpenNow();
  }

  /**
   * {@inheritdoc}
   */
  public function isOpenToday() {
    return $this->isOpenOnDate($this->getToday());
  }

  /**
   * {@inheritdoc}
   */
  public function isClosedToday() {
    return !$this->isOpenOnDate($this->getToday());
  }

  /**
   * {@inheritdoc}
   */
  public function isOpenOnDate(DrupalDateTime $date = NULL) {
    $opening_periods = $this->getOpeningPeriodsForDate($date);
    return !$opening_periods || empty($opening_periods) ? FALSE : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosedOnDate(DrupalDateTime $date = NULL) {
    return !$this->isOpenOnDate($date);
  }

  /**
   * {@inheritdoc}
   */
  public function getOpeningPeriodsForDate(DrupalDateTime $date = NULL) {
    $date = $date ? $date : $this->getToday();

    // First check if we have exceptions overriding the normal
    // opening days.
    foreach ($this->getBusinessExceptions() as $exception) {
      if (!$this->isSameDate($exception->getDate(), $date)) {
        continue;
      }

      return $exception->getPeriods();
    }

    // Then check the current day.
    // opening days.
    foreach ($this->getBusinessDays() as $business_day) {
      if (!$this->isSameDay(intval($date->format('w')), intval($business_day->getDay()))) {
        continue;
      }

      return $business_day->getPeriods();
    }

    return FALSE;
  }

  /**
   * Get the ordered weekdays.
   *
   * @return array
   *   An array of weekdays.
   */
  private function getOrderedWeekDays() {
    return $this->orderWeekday($this->firstDay);
  }

  /**
   * Prepare the business exceptions.
   *
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDateInterface[] $exceptions
   *   List of business date periods.
   *
   * @return \Drupal\d01_drupal_business_hours\BusinessDateInterface[]
   *   Ordered list of business dates by date.
   */
  private function prepareBusinessExceptions(array $exceptions = []) {

    // Bundle business exceptions by date.
    $bundled_business_exceptions = [];
    foreach ($exceptions as $exception) {
      // Build key based on year month day so we can group items by date.
      // By using this format we can also ksort the value by date.
      $date = $exception->getDate();
      $key = $date->format('Ymd');

      // Filter out passed dates we needed.
      if ($this->excludePastDates && $this->getToday()->getTimestamp() > $date->getTimestamp()) {
        continue;
      }

      $bundled_business_exceptions[$key]['date'] = $exception->getDate();
      $bundled_business_exceptions[$key]['periods'][] = $exception;
    }

    ksort($bundled_business_exceptions);

    // Create business days from business hours.
    $business_dates = [];
    foreach ($bundled_business_exceptions as $key => $value) {
      $business_dates[] = new BusinessDate($value['date'], $value['periods']);
    }

    return $business_dates;
  }

  /**
   * Prepare the business days.
   *
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDayInterface[] $business_hours
   *   List of business day periods.
   *
   * @return \Drupal\d01_drupal_business_hours\BusinessDayInterface[]
   *   Ordered list of business days by day.
   */
  private function prepareBusinessDays(array $business_hours = []) {
    // Bundle business hours by day.
    $bundled_business_hour = [];
    foreach ($business_hours as $business_hour) {
      // Build key based on day so we can group items by day.
      // By using this format we can also ksort the value by day.
      $day = $business_hour->getDay();
      $key = $day;

      $bundled_business_hour[$key][] = $business_hour;
    }

    // Fill in missing weekdays.
    $weekdays = $this->getWeekdays();
    foreach ($weekdays as $key => $weekday) {
      if (in_array($key, array_keys($bundled_business_hour))) {
        continue;
      }
      $bundled_business_hour[$key] = [];
    }

    ksort($bundled_business_hour);

    // Create business days from business hours.
    $business_days = [];
    foreach ($bundled_business_hour as $key => $bundled_period) {
      $business_days[] = new BusinessDay($key, $bundled_period);
    }

    return $business_days;
  }

}
