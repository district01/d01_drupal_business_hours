<?php

namespace Drupal\d01_drupal_business_hours;

/**
 * Interface BusinessDayPeriodInterface.
 *
 * @package Drupal\d01_drupal_business_hours
 */
interface BusinessPeriodInterface {

  /**
   * Get the From time.
   *
   * @return \Drupal\d01_drupal_time\D01DrupalTime
   *   a D01DrupalTime object which is as wrapper for the DrupalDateTime.
   */
  public function getFromTime();

  /**
   * Get the From time.
   *
   * @return \Drupal\d01_drupal_time\D01DrupalTime
   *   a D01DrupalTime object which is as wrapper for the DrupalDateTime.
   */
  public function getUntilTime();

  /**
   * Get the provided comment.
   *
   * @return string
   *   a text string.
   */
  public function getComment();

}
