<?php

namespace Drupal\d01_drupal_business_hours;

/**
 * Interface BusinessDayInterface.
 *
 * @package Drupal\d01_drupal_business_hours
 */
interface BusinessDayInterface {

  /**
   * Constructor.
   *
   * @param int $day
   *   Numeric representation of day.
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDayInterface[] $periods
   *   An array of periods of type 'day'.
   */
  public function __construct($day, array $periods);

  /**
   * Get the translated Day string.
   *
   * @param string $format
   *   The format the day should be in.
   *   Supported formats are abbr, abbr1, abbr2 and default.
   *
   * @return string
   *   The label of the day.
   */
  public function label($format = 'default');

  /**
   * Get the Day.
   *
   * @return int
   *   The numeric representation of the day.
   */
  public function getDay();

  /**
   * Get the periods of the Day.
   *
   * @return \Drupal\d01_drupal_business_hours\BusinessPeriodDayInterface[]
   *   An array of Business hours.
   */
  public function getPeriods();

  /**
   * Get the comments of the Day.
   *
   * @return array
   *   An array of Text strings.
   */
  public function getGroupedComments();

  /**
   * Check if the date is closed today (whole day).
   *
   * This function will only return TRUE, when there are
   * no periods for this date. It will also take
   * the passed exceptions into account. This function will
   * not check in real-time, use isOpenNow()
   * for that.
   *
   * @return bool
   *   Boolean indicating if this date is closed.
   */
  public function isClosedToday();

  /**
   * Check if the date is open today (whole day).
   *
   * This will only return TRUE, when there are
   * periods for this date. This function will
   * not check in real-time, use isOpenNow()
   * for that.
   *
   * @return bool
   *   Boolean indicating if this date is open today.
   */
  public function isOpenToday();

  /**
   * Check if the date is closed on this instance.
   *
   * This will check in real-time, to see if date is closed.
   *
   * @return bool
   *   Boolean indicating if this date is closed today.
   */
  public function isClosedNow();

  /**
   * Check if the date is open on this instance.
   *
   * This will check in real-time, to see if date is open.
   *
   * @return bool
   *   Boolean indicating if this date is open today.
   */
  public function isOpenNow();

}
