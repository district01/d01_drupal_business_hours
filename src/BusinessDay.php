<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Class BusinessHoursDay.
 *
 * @package Drupal\d01_drupal_business_hours
 */
class BusinessDay implements BusinessDayInterface {

  use BusinessHoursTrait;

  /**
   * Numeric representation of day.
   *
   * @var int
   */
  protected $day = NULL;

  /**
   * List of Business Hour objects.
   *
   * @var \Drupal\d01_drupal_business_hours\BusinessPeriodDayInterface[]
   */
  protected $periods = [];

  /**
   * {@inheritdoc}
   */
  public function __construct($day, array $periods = []) {
    $this->day = $day;
    $this->periods = $this->preparePeriods($periods);
  }

  /**
   * {@inheritdoc}
   */
  public function label($format = 'default') {
    switch ($format) {
      case 'abbr':
        $weekdays = DateHelper::weekDaysAbbr(TRUE);
        break;

      case 'abbr1':
        $weekdays = DateHelper::weekDaysAbbr1(TRUE);
        break;

      case 'abbr2':
        $weekdays = DateHelper::weekDaysAbbr2(TRUE);
        break;

      default:
        $weekdays = DateHelper::weekDays(TRUE);
    }

    return isset($weekdays[$this->day]) ? $weekdays[$this->day] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDay() {
    return (int) $this->day;
  }

  /**
   * {@inheritdoc}
   */
  public function getPeriods() {
    return $this->periods;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupedComments() {
    $comments = [];
    $periods = $this->getPeriods();

    foreach ($periods as $period) {
      $comments[] = $period->getComment();
    }

    return $comments;
  }

  /**
   * {@inheritdoc}
   */
  public function isOpenNow() {
    // When day is closed we can just return false.
    if ($this->isClosedToday()) {
      return FALSE;
    }

    // Loop periods.
    foreach ($this->getPeriods() as $period) {
      $from = $period->getFromTime()->getTimestamp();
      $until = $period->getUntilTime()->getTimestamp();

      if (($from <= $this->getNow()->getTimestamp()) && !($until < $this->getNow()->getTimestamp())) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosedNow() {
    return !$this->isOpenNow();
  }

  /**
   * {@inheritdoc}
   */
  public function isOpenToday() {
    return !$this->isClosedToday();
  }

  /**
   * {@inheritdoc}
   */
  public function isClosedToday() {
    return empty($this->getPeriods()) ? TRUE : FALSE;
  }

  /**
   * Prepare the business periods.
   *
   * @param \Drupal\d01_drupal_business_hours\BusinessPeriodDayInterface[] $periods
   *   List of business day periods.
   *
   * @return \Drupal\d01_drupal_business_hours\BusinessPeriodDayInterface[]
   *   Ordered list of business day periods by day.
   */
  private function preparePeriods(array $periods) {
    $ordered = [];

    // We make sure we always pass the period in the
    // correct chronological order.
    if (!empty($periods)) {
      foreach ($periods as $period) {

        // Make sure the period passed matches the day.
        if (!$this->isSameDay($period->getDay(), $this->getDay())) {
          continue;
        }

        // Set timestamp as key for easy sorting.
        $timestamp = $period->getFromTime()->getTimestamp();
        $ordered[$timestamp] = $period;
      }

      // Sort by timestamp.
      ksort($ordered);

      // Clean up the keys.
      $periods = [];
      $count = 0;
      foreach ($ordered as $key => $order) {
        $periods[$count] = $order;
        $count++;
      }

      $ordered = $periods;
    }
    return $ordered;
  }

}
