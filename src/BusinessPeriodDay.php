<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\d01_drupal_time\D01DrupalTime;
use Drupal\d01_drupal_business_hours\BusinessPeriod;

/**
 * Class BusinessPeriodDay.
 *
 * @package Drupal\d01_drupal_business_hours
 */
class BusinessPeriodDay implements BusinessPeriodDayInterface {

  /**
   * Numeric representation of day.
   *
   * @var int
   */
  protected $day = NULL;

  /**
   * Business period object.
   *
   * @var \Drupal\d01_drupal_business_hours\BusinessPeriod
   */
  protected $businessPeriod = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct($day, BusinessPeriod $business_period) {
    $this->day = $day;
    $this->businessPeriod = $business_period;
  }

  /**
   * {@inheritdoc}
   */
  public function getDay() {
    return (int) $this->day;
  }

  /**
   * {@inheritdoc}
   */
  public function getFromTime() {
    return $this->businessPeriod->getFromTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getUntilTime() {
    return $this->businessPeriod->getUntilTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getComment() {
    return $this->businessPeriod->getComment();
  }

}
