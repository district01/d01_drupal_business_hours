<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\DateHelper;

/**
 * Provides methods to get dates.
 */
trait BusinessHoursTrait {

  /**
   * Get the first day of the week.
   *
   * @return int
   *   An integer representing the day of the week [0, 1, 2, 3, 4, 5, 6].
   */
  private function getFirstDayOfWeek() {
    $config = \Drupal::config('system.date');
    return $config->get('first_day') ? intval($config->get('first_day')) : 0;
  }

  /**
   * Get all weekdays.
   *
   * @return mixed
   *   Array of weekdays.
   */
  private function getWeekdays() {
    return DateHelper::weekDays(TRUE);
  }

  /**
   * Get the current datetime.
   *
   * This is meant for time based calculations.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   A date time object.
   */
  private function getNow() {
    return new DrupalDateTime();
  }

  /**
   * Get the datetime of today.
   *
   * This is meant for date based calculations.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   A date time object.
   */
  private function getToday() {
    $today = new DrupalDateTime();
    $today->setTime(0, 0, 0);
    return $today;
  }

  /**
   * Compare 2 days.
   *
   * @param int $day1
   *   First day to compare.
   * @param int $day2
   *   Second day to compare.
   *
   * @return bool
   *   Boolean to indicate match.
   */
  private function isSameDay($day1, $day2) {
    return $day1 === $day2 ? TRUE : FALSE;
  }

  /**
   * Compare 2 dates.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date1
   *   First day to compare.
   * @param \Drupal\Core\Datetime\DrupalDateTime $date2
   *   Second day to compare.
   *
   * @return bool
   *   Boolean to indicate match.
   */
  private function isSameDate(DrupalDateTime $date1, DrupalDateTime $date2) {
    // Copy date object so we can modify it.
    $copy_passed_date = $date2;

    // Make sure timezones match.
    $timezone = $date1->getTimezone();
    $copy_passed_date->setTimezone($timezone);

    // Compare.
    return intval($copy_passed_date->format('Ymd')) === intval($date1->format('Ymd')) ? TRUE : FALSE;
  }

  /**
   * Ordered weekdays to match the first day of the week.
   *
   * @return array
   *   An array of weekdays ordered to match the first day of the week. The
   *   keys will remain unchanged. For example, if the first day of the week is
   *   set to be Monday, the array keys will be [1, 2, 3, 4, 5, 6, 0].
   */
  private function orderWeekday($first_day = FALSE) {
    $first_day = $first_day ? $first_day : $this->getFirstDayOfWeek();
    $weekdays = $this->getWeekdays();

    if ($first_day > 0) {
      for ($i = 1; $i <= $first_day; $i++) {

        // Reset the array to the first element.
        reset($weekdays);

        // Retrieve the first week day value.
        $last = current($weekdays);

        // Store the corresponding key.
        $key = key($weekdays);

        // Remove this week day from the beginning of the array.
        unset($weekdays[$key]);

        // Add this week day to the end of the array.
        $weekdays[$key] = $last;
      }
    }
    return $weekdays;
  }

}
