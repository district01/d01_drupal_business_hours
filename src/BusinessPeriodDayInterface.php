<?php

namespace Drupal\d01_drupal_business_hours;

/**
 * Interface BusinessDayPeriodInterface.
 *
 * @package Drupal\d01_drupal_business_hours
 */
interface BusinessPeriodDayInterface extends BusinessPeriodInterface {

  /**
   * Get numeric representation of Day.
   *
   * @return int
   *   numeric representation of the day.
   */
  public function getDay();

}
