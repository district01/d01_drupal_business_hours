<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\TypedData\TypedData;
use Drupal\d01_drupal_business_hours\Plugin\Field\FieldType\BusinessPeriodDayItem;
use Drupal\d01_drupal_time\D01DrupalTime;
use Drupal\d01_drupal_business_hours\BusinessPeriod;
use Drupal\d01_drupal_business_hours\BusinessPeriodDay;

/**
 * A computed property for periods.
 *
 * Required settings (below the definition's 'settings' key) are:
 *  - date source: The date property containing the to be computed date.
 */
class BusinessPeriodDayComputed extends TypedData {

  /**
   * Cached computed BusinessPeriodDay.
   *
   * @var \Drupal\d01_drupal_business_hours\BusinessPeriodDay|null
   */
  protected $businessPeriodDay = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    if (!$definition->getSetting('day')) {
      throw new \InvalidArgumentException("The definition's 'day' key has to specify the name of the day property.");
    }
    if (!$definition->getSetting('from')) {
      throw new \InvalidArgumentException("The definition's 'from' key has to specify the name of the from property.");
    }
    if (!$definition->getSetting('until')) {
      throw new \InvalidArgumentException("The definition's 'until' key has to specify the name of the until property.");
    }
    if (!$definition->getSetting('comment')) {
      throw new \InvalidArgumentException("The definition's 'comment' key has to specify the name of the comment property.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if ($this->businessPeriodDay !== NULL) {
      return $this->businessPeriodDay;
    }

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    $item = $this->getParent();
    $day = $item->{($this->definition->getSetting('day'))};
    $from = $item->{($this->definition->getSetting('from'))};
    $until = $item->{($this->definition->getSetting('until'))};
    $comment = $item->{($this->definition->getSetting('comment'))};

    $business_period = new BusinessPeriod($from, $until, $comment);
    $this->businessPeriodDay = new BusinessPeriodDay($day, $business_period);

    return $this->businessPeriodDay;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    $this->businessPeriodDay = $value;
    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->businessPeriodDay);
    }
  }

}
