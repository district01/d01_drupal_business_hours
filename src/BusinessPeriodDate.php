<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\d01_drupal_business_hours\BusinessPeriod;

/**
 * Class BusinessPeriodDate.
 *
 * @package Drupal\d01_drupal_business_hours
 */
class BusinessPeriodDate implements BusinessPeriodDateInterface {

  /**
   * Drupal date time object.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime
   */
  protected $date = NULL;

  /**
   * Business period object.
   *
   * @var \Drupal\d01_drupal_business_hours\BusinessPeriod
   */
  protected $businessPeriod = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(DrupalDateTime $date, BusinessPeriod $business_period) {
    $this->date = $date;
    $this->businessPeriod = $business_period;
  }

  /**
   * {@inheritdoc}
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * {@inheritdoc}
   */
  public function getFromTime() {
    return $this->businessPeriod->getFromTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getUntilTime() {
    return $this->businessPeriod->getUntilTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getComment() {
    return $this->businessPeriod->getComment();
  }

}
