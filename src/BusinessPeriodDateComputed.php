<?php

namespace Drupal\d01_drupal_business_hours;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\TypedData\TypedData;
use Drupal\d01_drupal_business_hours\Plugin\Field\FieldType\BusinessPeriodDayItem;
use Drupal\d01_drupal_time\D01DrupalTime;
use Drupal\d01_drupal_business_hours\BusinessPeriod;
use Drupal\d01_drupal_business_hours\BusinessPeriodDate;

/**
 * A computed property for periods.
 *
 * Required settings (below the definition's 'settings' key) are:
 *  - date source: The date property containing the to be computed date.
 */
class BusinessPeriodDateComputed extends TypedData {

  /**
   * Cached computed BusinessPeriodDate.
   *
   * @var \Drupal\d01_drupal_business_hours\BusinessPeriodDate|null
   */
  protected $businessPeriodDate = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    if (!$definition->getSetting('date')) {
      throw new \InvalidArgumentException("The definition's 'date' key has to specify the name of the date property.");
    }
    if (!$definition->getSetting('comment')) {
      throw new \InvalidArgumentException("The definition's 'comment' key has to specify the name of the comment property.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if ($this->businessPeriodDate !== NULL) {
      return $this->businessPeriodDate;
    }

    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    $item = $this->getParent();

    // Handle require definitions.
    $date = $item->{($this->definition->getSetting('date'))};
    $comment = $item->{($this->definition->getSetting('comment'))};

    // Handle optional definitions.
    $from_definition = $this->definition->getSetting('from') ? $this->definition->getSetting('from') : NULL;
    $until_definition = $this->definition->getSetting('until') ? $this->definition->getSetting('until') : NULL;
    $from = $from_definition ? $item->{($from_definition)} : NULL;
    $until = $until_definition ? $item->{($until_definition)} : NULL;

    $business_period = new BusinessPeriod($from, $until, $comment);
    $this->businessPeriodDate = new BusinessPeriodDate($date, $business_period);

    return $this->businessPeriodDate;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    $this->businessPeriodDate = $value;
    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->businessPeriodDate);
    }
  }

}
