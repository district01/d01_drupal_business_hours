<?php

namespace Drupal\d01_drupal_business_hours;

/**
 * Interface BusinessPeriodDateInterface.
 *
 * @package Drupal\d01_drupal_business_hours
 */
interface BusinessPeriodDateInterface extends BusinessPeriodInterface {

  /**
   * Get the Date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   a drupal date time object.
   */
  public function getDate();

}
